from django.urls import include, path
from django.conf.urls import include

from rest_framework.routers import SimpleRouter

from api.views import LogRecordViewSet

router = SimpleRouter()
router.register(r'log_records', LogRecordViewSet, basename='log_records')

urlpatterns = [
    path(r'', include(router.urls)),
]
