from rest_framework import viewsets

from api.models import LogRecord
from api.serializers import LogRecordSerializer


class LogRecordViewSet(viewsets.ModelViewSet):
    serializer_class = LogRecordSerializer
    queryset = LogRecord.objects.all()