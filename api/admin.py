from django.contrib import admin
from api.models import \
    (Counterparty,
     LogRecord,
     LogRecordItem,
     Warehouse)


class LogRecordItemAdmin(admin.ModelAdmin):
    fields = ('log_record', 'current_serial_number', 'new_serial_number', 'item_name',
              'item_code', 'logical_warehouse')
    list_display = ('id', 'log_record', 'current_serial_number', 'new_serial_number', 'item_name',
                    'item_code', 'logical_warehouse')
    list_editable = ('log_record', 'current_serial_number', 'new_serial_number', 'item_name',
                     'item_code', 'logical_warehouse')


admin.site.register(LogRecordItem, LogRecordItemAdmin)


class LogRecordAdmin(admin.ModelAdmin):

    fields = ('number', 'counterparty', 'warehouse', 'wms_fullname', 'executor',
              'created_at', 'approved_at')
    list_display = ('id', 'number', 'counterparty', 'warehouse', 'wms_fullname', 'executor',
                    'created_at', 'approved_at')
    list_editable = ('number', 'counterparty', 'warehouse', 'wms_fullname', 'executor',
                     'created_at', 'approved_at')


admin.site.register(LogRecord, LogRecordAdmin)


class WarehouseAdmin(admin.ModelAdmin):

    fields = ('name',)


admin.site.register(Warehouse, WarehouseAdmin)


class CounterpartyAdmin(admin.ModelAdmin):

    fields = ('name',)


admin.site.register(Counterparty, CounterpartyAdmin)
