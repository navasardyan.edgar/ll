from rest_framework import serializers

from api.models import LogRecord


class LogRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model=LogRecord
        fields = ('id', 'number', 'counterparty', 'warehouse', 'wms_fullname', 'executor', 'number_of_records', 'created_at',
                  'approved_at')

