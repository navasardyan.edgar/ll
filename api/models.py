from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

STATUS_CHOICES = (
    ('1', _('Started')),
    ('2', _('Ended')),
)


class Counterparty(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'counterparty'

    def __str__(self):
        return self.name


class Warehouse(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'warehouse'

    def __str__(self):
        return self.name


class LogRecord(models.Model):
    number = models.CharField(max_length=128)
    counterparty = models.ForeignKey(Counterparty, on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    wms_fullname = models.CharField(max_length=1, choices=STATUS_CHOICES, default='1')
    executor = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(max_length=128)
    approved_at = models.DateField(max_length=128)

    class Meta:
        db_table = 'log_record'

    def __str__(self):
        return self.number

    def number_of_records(self):
        return self.items.count()


class LogRecordItem(models.Model):
    log_record = models.ForeignKey(LogRecord, on_delete=models.CASCADE, related_name='items')
    current_serial_number = models.CharField(max_length=128)
    new_serial_number = models.CharField(max_length=128)
    item_name = models.CharField(max_length=128)
    item_code = models.CharField(max_length=128)
    logical_warehouse = models.CharField(max_length=128)

    class Meta:
        db_table = 'log_record_item'

    def __str__(self):
        return self.item_name
