from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.utils.translation import gettext as _

from rest_framework import serializers
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings

from authentication.models import UserProfile

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class CustomJWTSerializer(JSONWebTokenSerializer):
    username_field = 'login'

    def validate(self, attrs):

        password = attrs.get("password")
        user_obj = User.objects.filter(username=attrs.get("login")).first()
        if user_obj is not None:
            credentials = {
                'username': user_obj.username,
                'password': password
            }

            if all(credentials.values()):
                user = authenticate(**credentials)
                if user:
                    if not user.is_active:
                        msg = _('User account is disabled.')
                        raise serializers.ValidationError(msg)

                    payload = jwt_payload_handler(user)

                    return {
                        'token': jwt_encode_handler(payload),
                        'user': user
                    }
                else:
                    msg = _('Unable to log in with provided credentials.')
                    raise serializers.ValidationError(msg)

            else:
                msg = _('Must include "{username_field}" and "password".')
                msg = msg.format(username_field=self.username_field)
                raise serializers.ValidationError(msg)

        else:
            msg = _('Account with such WMS identifier does not exists')
            raise serializers.ValidationError(msg)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('password',)
        extra_kwargs = {
            'password': {'write_only': True},
            'id': {'read_only': True},
        }

    def to_internal_value(self, data):

        return super().to_internal_value(data)


class UserProfileSerializer(serializers.ModelSerializer):

    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = ('id', 'user', 'wms_number', 'wms_fullname')

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_data['username'] = validated_data.pop('wms_number')
        user = User.objects.create_user(**user_data)
        profile = UserProfile.objects.create(user=user, wms_number=user_data['username'], **validated_data)
        return profile
