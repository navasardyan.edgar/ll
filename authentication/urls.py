from django.urls import include, path

from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import ObtainJSONWebToken
from rest_framework.urlpatterns import format_suffix_patterns


from authentication.views import UserViewSet, GetUserDetails
from authentication.serializers import CustomJWTSerializer


router = SimpleRouter()
router.register(r'users', UserViewSet, basename='users')


urlpatterns = [
    path('', include(router.urls)),
    path('login/', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer)),
    path('user_details/', GetUserDetails.as_view(), name='user-details'),
]

urlpatterns = format_suffix_patterns(urlpatterns)