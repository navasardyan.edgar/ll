from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet
from authentication.models import UserProfile
from authentication.serializers import (UserProfileSerializer)
from rest_framework import generics


class UserViewSet(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    queryset = UserProfile.objects.filter(user__is_active=True)
    serializer_class = UserProfileSerializer
    permission_classes = ()


class GetUserDetails(generics.RetrieveAPIView):
    serializer_class = UserProfileSerializer

    def get_object(self):
        obj = UserProfile.objects.get(user=self.request.user)
        return obj
