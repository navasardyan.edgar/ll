from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    wms_number = models.IntegerField()
    wms_fullname = models.CharField(max_length=255, default='rub')

    class Meta:
        db_table = 'user_profile'

    def __str__(self):
        return self.user.email
